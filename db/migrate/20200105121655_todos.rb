class Todos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.text :description
      t.boolean :completed
      t.integer :priority
      t.timestamps null: false
      t.integer :list_id
    end
  end
end
