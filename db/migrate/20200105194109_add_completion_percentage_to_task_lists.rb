class AddCompletionPercentageToTaskLists < ActiveRecord::Migration
  def change
  	add_column :task_lists, :completion_percentage, :float, default: 0.0
  end
end
