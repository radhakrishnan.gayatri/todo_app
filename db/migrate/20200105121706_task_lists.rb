class TaskLists < ActiveRecord::Migration
  def change
    create_table :task_lists do |t|
      t.text :name
      t.boolean :completed
      t.integer :priority
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
