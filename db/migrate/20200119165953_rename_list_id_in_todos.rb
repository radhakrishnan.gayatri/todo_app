class RenameListIdInTodos < ActiveRecord::Migration
  def change
  	rename_column :todos, :list_id, :task_list_id
  end
end
