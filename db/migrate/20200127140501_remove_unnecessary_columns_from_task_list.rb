class RemoveUnnecessaryColumnsFromTaskList < ActiveRecord::Migration
  def change
  	remove_column :task_lists, :priority
  	remove_column :task_lists, :completed
  end
end
