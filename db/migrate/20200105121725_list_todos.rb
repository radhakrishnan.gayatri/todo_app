class ListTodos < ActiveRecord::Migration
  def change
    create_table :list_todos do |t|
      t.integer :list_id
      t.integer :todo_id
      t.timestamps null: false
    end
  end
end
