module ApplicationHelper


  # def render_with_status_and_message(status:, message:, type:)
  #   if type.to_sym == :json

  #   else
  #   end
  # end
  def sortable(column, title = nil)
	  title ||= column.titleize
	  css_class = column == sort_column ? "current #{sort_direction}" : nil
	  direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
	  link_to title, {:sort => column, :direction => direction}, {:class => css_class}
	end
end
