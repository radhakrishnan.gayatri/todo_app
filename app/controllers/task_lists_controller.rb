class TaskListsController < ApplicationController
  before_action :find_task, only: [:edit, :update, :show, :destroy]

  # GET /task_lists
  def index
    @tasks = current_user.task_lists
  end

  # GET /task_lists/:id
  def show
    @todos = @task.ordered_todos(sort_column,sort_direction)
  end

  # GET /task_lists/new
  def new
    @task = TaskList.new
    @task.todos.build
  end

  # GET /task_lists/:id/edit
  def edit
    @task = TaskList.find_by(id: params[:id])
    @todos = @task.ordered_todos(sort_column,sort_direction)
    render template: 'task_lists/show', format: :html
  end

  # PUT /task_lists/:id
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to action: "show", notice: 'Task was successfully updated.'}
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /task_lists
  def create
    @task = current_user.task_lists.new(name: params[:task_list][:name])
    respond_to do |format|
      if @task.save
        format.html { redirect_to action: "index", notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /task_lists/:id
  def destroy
    respond_to do |format|
      if @task.destroy
        format.html { redirect_to action: "index", notice: 'Task was successfully deleted.' }
        format.json { render :index, status: :created, location: @task }
      else
        format.html { render :index }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def task_params
    params.require(:task_list).permit(:name, :priority,
      todos_attributes: [:id, :description,
        :completed, :priority,
        :_destroy, :task_list_id,
        :due_date])
  end

  def find_task
    @task = TaskList.find_by(id: params[:id])
  end
end