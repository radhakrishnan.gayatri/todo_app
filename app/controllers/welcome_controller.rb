class WelcomeController < ApplicationController
	# skip_filter :authenticate_user!

	def index
		if current_user
			@user = current_user
			@tasks = @user.task_lists
		end
		render template: 'after_login'
	end
end