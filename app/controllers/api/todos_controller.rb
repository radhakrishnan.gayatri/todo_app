class Api::TodosController < Api::BaseController
	before_action :find_task, only: [:create, :index, :update, :destroy, :show]
	before_action :find_todo, only: [:update, :destroy, :show]

	## GET /api/task_lists/:task_list_id/todos

  # api :GET, '/api/task_lists/:task_list_id/todos', 'Get todos for a task_list'
  # formats ['json']
  def index
  	@todos = @task.ordered_todos(sort_column, sort_direction)
    render template: 'task_lists/show', format: :json
  end

  ## GET /api/task_lists/:task_list_id/todos/:id

  # api :GET, '/api/task_lists/:task_list_id/todos/:id', 'Get todo for a task_list'
  # formats ['json']
  def show
	end

  ## POST /api/task_lists/:task_list_id/todos

  # api :PUT, 'api/task_lists/:task_list_id/todos', 'Create a task_list todo'
  # param :description, String, desc: 'Todo description', required: true
  # param :due_date, String, desc: "Due date with format mm/dd/yyyy", required: false
  # param :completed, String, desc: "boolean value indicating if todo is completed", required: false
  # formats ['json']
  def create
  	@todo = @task.todos.new(todo_params)
		if @todo.save
      render template: 'task_lists/show', format: :json
    else
      render json: @todo.errors, status: :unprocessable_entity
    end
  end

  ## PUT api/task_lists/:task_list_id/todos/:id

  # api :PUT, 'api/task_lists/:task_list_id/todos/:id', 'Update a task_list todo'
  # param :description, String, desc: 'Todo description', required: true
  # param :due_date, String, desc: "Due date with format mm/dd/yyyy", required: false
  # param :completed, String, desc: "boolean value indicating if todo is completed", required: false
  # param :priority, String, desc: "Priority of the task", required: false
  # formats ['json']
	def update
		@todo = @task.todos.find_by(id: params[:id])
		if @todo.update(todo_params) && @todo.reload
      render template: 'task_lists/show', format: :json
    else
      render json: @todo.errors, status: :unprocessable_entity
    end
	end

	# DELETE api/task_lists/:task_list_id/todos/:id

  # api :DELETE, 'api/task_lists/:task_list_id/todos/:id', 'Delete a task_list todo'
  # formats ['json']
	def destroy
		@todo = @task.todos.find_by(id: params[:id])
		if @todo.destroy
			render json: {message: 'Task Todo successuflly deleted'}, format: :json
		else
			render json: @task.errors, status: :unprocessable_entity
		end
	end

	private
	def find_task
    @task = TaskList.find_by(id: params[:task_list_id], user_id: @current_user.id)
    unless @task.present?
      render json: {message: 'Task not found'}, status: :not_found
    end
  end

  def find_todo
  	@todo = @task.todos.find_by(id: params[:id])
    unless @todo.present?
      render json: {message: 'Todo not found'}, status: :not_found
    end
  end

  def todo_params
  	params.require(:todos).permit(:id, :description, :priority, :completed, :due_date)
  end
end