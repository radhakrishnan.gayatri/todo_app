class Api::BaseController < ActionController::Base
  before_filter :authenticate_user_from_token!
  skip_before_filter :verify_authenticity_token # No need to check for CSRF token for json apis
  protect_from_forgery with: :null_session

  helper_method :sort_column, :sort_direction

  private
  def authenticate_user_from_token!
    api_token = request.headers["HTTP_API_TOKEN"]
    if !api_token
      render json: {message: 'User not authorized'}, status: :unauthorized
    else
      @user = nil
      payload, header = JWT.decode(api_token, User::TOKEN_SECRET_BASE)
      unless @current_user = User.find_by(id: payload['id'])
        render json: {message: 'User not found'}, status: :not_found
      end
    end
  end

  def sort_column
    Todo.column_names.include?(params[:sort]) ? params[:sort] : "priority"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end