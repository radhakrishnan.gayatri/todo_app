class Api::TaskListsController < Api::BaseController
  before_action :find_task, only: [:update, :show, :destroy]

  ## GET api/task_lists/index

  # api :GET, 'api/task_lists/index', 'Get users task_lists'
  # formats ['json']
  def index
    @tasks = TaskList.where(user_id: @current_user.id)
    render template: 'task_lists/index', format: :json
  end

  ## GET api/task_lists/:id

  # api :GET, 'api/task_lists/:id', 'Get details of a task_list'
  # param :direction, String, desc: 'search query string', required: false
  # param :sort, String, desc: 'search query string', required: false
  # formats ['json']
  def show
    @todos = @task.ordered_todos(sort_column, sort_direction)
    render template: 'task_lists/show', format: :json
  end

  ## PUT api/task_lists/:id
  # api :PUT, 'api/task_lists/:id', 'Update a task_list'

  # param :name, String, desc: 'Task List name', required: true
  # param :todo_attributes, Hash, desc: 'todo object' do
  #   param :description, String, desc: 'Todo description', required: true
  #   param :due_date, String, desc: "Due date with format mm/dd/yyyy", required: false
  # end
  # formats ['json']
  def update
    if @task.update(allowed_update_params) && @task.reload
      render template: 'task_lists/show', format: :json
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  # DELETE api/task_lists/:id

  # api :DELETE, 'api/task_lists/:id', 'Delete a task_list'
  # formats ['json']
  def destroy
    if @task.destroy
      render json: {message: 'Task successuflly deleted'}, format: :json
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  private

  def task_params
    params.require(:task_list).permit(:name,
      todos_attributes: [:id, :description,
        :completed, :priority,
        :_destroy, :task_list_id,
        :due_date])
  end

  def allowed_update_params
    params.require(:task_list).permit(:name,
      todos_attributes: [:id, :description, :completed, :priority, :due_date])
  end

  def find_task
    @task = TaskList.find_by(id: params[:id], user_id: @current_user.id)
    unless @task.present?
      render json: {message: 'Task not found'}, status: :unprocessable_entity
    end
  end
end