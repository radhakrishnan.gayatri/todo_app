class TodosController < ApplicationController
  before_action :find_task, only: [:new, :create, :index]

  # GET /task_lists/:task_list_id/todos/new
  def new
    render template: 'todo/new', format: :html
  end

  # GET /task_lists/:task_list_id/todos
  def index
  end

  # POST /task_lists/:task_list_id/todos
  def create
    @task.todos.create(description: todo_params[:description])
    @todos = @task.ordered_todos(sort_column,sort_direction)
    render template: 'task_lists/show', format: :html
  end

  private

  def todo_params
    params.require(:todo).permit(
      [:id, :task_list_id, :description, :completed, :priority, :due_date]
    )
  end

  def find_task
    @task = TaskList.find_by(id: params[:task_list_id])
  end
end