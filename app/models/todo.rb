class Todo < ActiveRecord::Base
  belongs_to :task_list
  before_create :set_priority

  validates_presence_of :description
  validates_presence_of :task_list_id

  after_commit :update_completion_percentage, on: [:create, :update]

  def update_completion_percentage
  	return unless (previous_changes['completed'].present? || previous_changes['id']&[0].nil?)
  	return if (previous_changes['completed'].present? &&
      (previous_changes['completed'][0].nil? && previous_changes['completed'][1].nil?)
      )

    completed_todos = task_list.todos.where(completed: 1).count.to_f
    total_todos = task_list.todos.count.to_f

    task_list.update(completion_percentage: (completed_todos/total_todos))
  end

  def set_priority
    self.priority = (task_list.todos.order('priority DESC').first&.priority || 0) + 1
  end
end
