require 'active_support/concern'

module TaskListable
  extend ActiveSupport::Concern
  included do
    def ordered_todos(sort_column, sort_direction)
      self.todos.order(sort_column + " " + sort_direction)
    end
  end
end
