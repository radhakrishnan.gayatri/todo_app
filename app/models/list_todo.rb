class ListTodo < ActiveRecord::Base
	belongs_to :task_list, foreign_key: 'list_id', class_name: 'TaskList'
  belongs_to :todo
end
