require 'jwt'
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  TOKEN_SECRET_BASE = 'c7d1abf1e4557f97bde2359c7634b56e'.freeze
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :validatable

  ##### Validations
  validates_presence_of   :email
  validates_presence_of   :first_name
  validates_presence_of   :last_name
  validates :email, uniqueness: { case_sensitive: false },
  	format: { with: Devise.email_regexp, message: 'is not a valid email address' }


  ##### Associations
  has_many :task_lists

  def generate_jwt
    JWT.encode({ id: id,
      exp: 7.days.from_now.to_i },
      TOKEN_SECRET_BASE
    )
  end
end
