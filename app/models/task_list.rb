class TaskList < ActiveRecord::Base
	include TaskListable
  has_many :todos
  belongs_to :user
  accepts_nested_attributes_for :todos, allow_destroy: true

  validates_presence_of :user_id
  validates_presence_of :name
end
