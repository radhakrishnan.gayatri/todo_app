json.extract! todo, :id, :task_list_id, :description, :completed,
  :priority, :due_date, :created_at, :updated_at

