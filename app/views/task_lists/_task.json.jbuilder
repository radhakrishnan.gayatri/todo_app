json.extract! task, :id, :name, :user_id, :created_at, :updated_at
json.completion_percentage task.completion_percentage*100
json.total_todos task.todos.count
