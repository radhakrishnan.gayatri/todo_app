json.task @task, partial: "task_lists/task", as: :task
json.todos do
  json.array! (@todos || @task.todos), partial: "todo/todo", as: :todo
end
